"use strict";

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

function createList(books) {
  const ul = document.createElement("ul");
  books.forEach((item) => {
    const li = document.createElement("li");
    try {
      if (!item.author) {
        throw new Error(
          "author is undefined"
          // console.log(`Error! author  is ${item.author}`)
        );
      } else if (!item.price) {
        throw new Error(
          "price is undefined"
          // console.log(`Error! price  is ${item.price}  `)
        );
      } else if (item.author && item.name && item.price) {
        li.textContent = ` ${item.author} " ${item.name}"  ${item.price} `;
        ul.appendChild(li);
      }
    } catch (e) {
      console.log(e);
    }
  });
  return ul;
}
document.getElementById("root").appendChild(createList(books));

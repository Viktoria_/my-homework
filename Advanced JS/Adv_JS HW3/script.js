"use strict";

class Employee {
  constructor(options) {
    this.name = options.name;
    this.age = options.age;
    this.salary = options.salary;
  }
  set name(value) {
    this._name = value;
  }
  get name() {
    return this._name;
  }
  set age(value) {
    this._age = value;
  }
  get age() {
    return this._age;
  }
  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary;
  }
}
const employee = new Employee({
  name: "Anna",
  age: 33,
  salary: 100,
});
console.log(employee);

class Programmer extends Employee {
  constructor(options) {
    super(options);
    this.lang = options.lang;
  }

  get salaryProg() {
    return this.salary * 3;
  }

  set salaryProg(newSalary) {
    this.salary = newSalary;
  }
}

const programmer = new Programmer({
  name: "Nina",
  age: 37,
  salary: 200,
  lang: "eng",
});
console.log(programmer);
console.log((programmer.salaryProg = 300));
console.log(programmer.salaryProg);

const progr2 = programmer;
console.log(progr2);

const progr3 = programmer;
console.log(progr3);



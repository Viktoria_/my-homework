"use strict";

const URI = "https://swapi.dev/api/films";

async function getFilms() {
  const response = await fetch(URI, {
    method: "GET",
  });
  const data = await response.json();
  return data;
}

function getChar(charURI) {
  return fetch(charURI).then((response) => response.json());
}

async function getCharacters(characters, ul) {
  let arr = [];
  let arrPromises = [];
  characters.forEach((e) => {
    const li = document.createElement("li");
    arrPromises.push(
      getChar(e).then(({ name }) => {
        li.textContent = name;
      })
    );
    arr.push(li);
  });
  console.log(arrPromises);

  await Promise.all([...arrPromises]);

  const li = document.createElement("li");
  li.textContent = "characters : ";
  li.style.color = "red";
  ul.append(li);

  const character = document.createElement("ul");
  li.append(character);
  character.append(...arr);
  return false;
}

const rootEl = document.getElementById("root");

getFilms()
  .then(({ results }) => {
    results.forEach(
      ({
        title: film,
        episode_id: id,
        opening_crawl: description,
        characters,
      }) => {
        const ul = document.createElement("ul");
        rootEl.append(ul);
        let filmItems = { film, id, description };

        for (let key in filmItems) {
          const li = document.createElement("li");
          li.textContent = key + " : " + filmItems[key];
          ul.append(li);
        }
        getCharacters(characters, ul);
      }
    );
  })
  .catch((error) => {
    console.log(error.message);
  });

"use strict";

const btn = document.getElementById("button-ip");
btn.addEventListener("click", getIpAdress);

function getIpAdress() {
  const URI = "https://api.ipify.org/?format=json";

  async function getIP() {
    const response = await fetch(URI);
    const data = await response.json();
    return data;
  }

  getIP()
    .then(({ ip }) => {
      async function getAdress() {
        const URIApi = "http://ip-api.com/";
        const info =
          "?fields=status,message,continent,country,region,city,district";
        const responseIP = await fetch(URIApi + "json/" + ip + info);
        const dataIP = await responseIP.json();
        return dataIP;
      }

      getAdress().then(({ continent, country, region, city, district }) => {
        const ul = document.createElement("ul");
        btn.after(ul);
        let items = { continent, country, region, city, district };

        for (let key in items) {
          const li = document.createElement("li");
          li.textContent = key + " : " + items[key];
          ul.append(li);
        }
      });
    })
    .catch((error) => {
      console.log(error.message);
    });
}

// console.log();

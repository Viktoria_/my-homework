let button = document.querySelector(".nav-button"),
  menuHidden = document.querySelector(".nav-menu"),
  menuItems = document.querySelectorAll(".nav-menu__item");

button.addEventListener("click", function (e) {
  button.classList.toggle("nav-button--active");
  menuHidden.classList.toggle("nav-menu--active");
  menuItems.classList.toggle("nav-menu--active");
});
;

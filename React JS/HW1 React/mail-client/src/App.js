import React, { Component } from "react";
import './App.scss';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

const modals = [
  {header: "Do you want to delete this file?",
   text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
   closeButton: true,
   buttonText: {
     okButton: "Ok",
     cancelButton: "Cancel"
   }
  },
  {header: "Save file?",
   text: "Are you sure?",
   closeButton: false,
   buttonText: {
    okButton: "Yes",
    cancelButton: "No"
  }
  }
]

class App extends Component {
  state = {
    currentModal: null,
    isActive: false
  }

  openModal = ({id}) => {
    this.setState({ currentModal : modals[id]})
    this.setState({ isActive : true})
  }

  closeModal = () => {
    this.setState({ isActive : false})
  }

  render() {
    const {currentModal, isActive} = this.state
    return (
      <div className="App">
        <div className="mainButtons">
          <Button
            backgroundColor="yellow"
            text="Open first modal"
            handleClick={this.openModal}
            id={0}
            classButton="buttons"
          />
          <Button
            backgroundColor="lightblue"
            text="Open second modal"
            handleClick={this.openModal}
            id={1}
            classButton="buttons"
          />
        </div>
        
        {isActive && <Modal
          header={currentModal.header}
          text={currentModal.text}
          closeButton={currentModal.closeButton}
          actions={{
            okButton: () => (<Button 
            text={currentModal.buttonText.okButton}
            handleClick={this.closeModal}
            classButton="buttons-Ok-Cancel--button"/>),

            cancelButton: () => (<Button
              text={currentModal.buttonText.cancelButton}
              handleClick={this.closeModal}
              classButton="buttons-Ok-Cancel--button"/>)
            }}
          isActive={isActive}
          closeModal={this.closeModal}
        />}
      </div>
    );
  }
}

export default App;


import React, { Component } from 'react';
import './Modal.scss';
import Button from "../Button/Button";

class Modal extends Component {
  render() {
    const {header, closeButton, text, actions, closeModal} = this.props
    
    return (
      <div className="Modal-window" onClick={() => closeModal()}>
        <div className="Modal" onClick={(e) => e.stopPropagation()}>
          <div className="Modal--header">
            <p>{header}</p>
            {closeButton && <Button
                classButton="closeButton"
                backgroundColor="red"
                text="X"
                handleClick={closeModal}
                />}
          </div>  
          <div className="Modal--body">
              {text.split('. ').map((el, index) => {
                  return (
                      <p key={index}>
                          {el}
                          {index !== (text.split('. ').length - 1 ) && "."}
                      </p>
                  )
              })
            }
          </div> 
          <div className="Modal--buttons-Ok-Cancel buttons-Ok-Cancel">
              {actions.okButton()}
              {actions.cancelButton()}
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;

import React, { Component } from 'react';
import './App.scss';
import axios from 'axios';
import Loading from './components/Loading/Loading';
import Header from './components/Header/Header'
import Main from './components/Main/Main'

const currentModal = {
  header: "Do you want to add this cake to cart?",
  text: "Nice choose! Are you sure?",
  closeButton: true,
  buttonText: {
      confirmButton: "Yes",
      cancelButton: "Cancel"
  }
}

class App extends Component {
  state = {
    cards: [],
    isLoading: true,
    isActive: false,
    card: {},
  }

  componentDidMount() {
    axios('http://localhost:3000/items.json')
      .then(result => {
        this.updateCards(this.normalizeData(result.data.items))
        this.updateLoading(false)
      })
  }

  normalizeData = (data) => {
    return data.map(el => {
      const favorites = JSON.parse(localStorage.getItem('favorites')) || []
      el.isFavorite = favorites.includes(el.vendorCode)
      
      return el
    })
  }
  
  
  updateCards = (data) => {
    this.setState({cards: data})
  }
 
  updateLoading = (data) => {
    this.setState(() => ({isLoading: data})) 
  }

  openModal = (item) => {
    this.setState({ card : item })
    this.setState(() => ({ isActive : true }))
  }

  closeModal = () => {
    this.setState({ isActive : false})
  }

  addToCart = () => {
    const {card, cards} = this.state
    this.setState({ isActive : false})
      
    const newCards = cards.map(el => {
      if (el.vendorCode === card.vendorCode) {
        el.amountAtCart++
      }
      return el
    })
    this.setState({cards: newCards})
    let cart = JSON.parse(localStorage.getItem("cart")) || []
    cart = newCards.filter(el => el.amountAtCart !== 0)
    localStorage.setItem('cart', JSON.stringify(cart)) 
  }

  addToFavorites = (card => {
    const {cards} = this.state
    const newCards = cards.map(el => {
      if (el.vendorCode === card.vendorCode) {
        el.isFavorite = !el.isFavorite
      }
      return el
    })
    this.setState({cards: newCards})

    let favorites = JSON.parse(localStorage.getItem("favorites")) || []
    favorites = (favorites.includes(card.vendorCode) ? favorites.filter(el => el !== card.vendorCode) : favorites.concat(card.vendorCode))
    localStorage.setItem("favorites", JSON.stringify(favorites))
    
  })  

  render() {
    const {cards, isLoading, isActive} = this.state
    
    if (isLoading) {
      return <Loading/>
    } 

    return (
      <div className="App">
        <Header />
        <Main cards={cards} 
              openModal={this.openModal} 
              closeModal={this.closeModal}
              addToCart={this.addToCart} 
              isActive={isActive} 
              currentModal={currentModal}
              addToFavorites={this.addToFavorites}
        />
      </div>
    );
  }
}

export default App;

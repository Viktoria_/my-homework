import React, { Component } from "react";
import Button from "../Button/Button";
import Icon from "../Icon/Icon";
import "./Card.scss";
import PropTypes from "prop-types"

class Card extends Component {
    render() {
      const {card, openModal, addToFavorites} = this.props
      return (
        <div className="card">
              <img src={card.url} alt={card.title} width='240' height='290'></img>
              <span className="Card--title">{card.title}</span>
              <span>{card.vendorCode}</span>
              <span>{card.color}</span>
              
              
              <span className="Card--price">{card.price} UAH</span>
              <Button
                  backgroundColor="darkviolet"
                  text="Add to cart"
                  handleClick={() => openModal(card)}
                  classButton="Card--addToCartButton"    
              />
                            
              <div className="Card--favorites">
                <span className="favorites--text">Add to favorites</span>
                <Icon
                  onClick={() => addToFavorites(card)}
                  filled={card.isFavorite}
                  color="darkviolet"
                  className="favorites--icon"
                />
              </div>
        </div>        
      );
    }
  }
  
  export default Card;
  
 Card.propTypes = {
    card: PropTypes.exact({
      title: PropTypes.string,
      price: PropTypes.number,
      url: PropTypes.string,
      vendorCode: PropTypes.number,
      color: PropTypes.string,
      amountAtCart: PropTypes.number,
      isFavorite: PropTypes.bool
    }).isRequired,
    openModal: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired
  }
  
//   class Card extends Component {
//     render() {
//       const {card, openModal, addToFavorites} = this.props
//       return (
//         <div className="card">
//               <img src={card.url} alt={card.title} width='200' height='300'></img>
//               <span>Card title: <b>{card.title}</b></span>
//               <span>Vendor code: <i>{card.vendorCode}</i></span>
//               <span>Card color: {card.color}</span>
//               <div className="Card--favorites favorites">
//                 <span className="Card--title">Add to favorites</span>
//                 <Icon
//                   onClick={() => addToFavorites(card)}
//                   filled={card.isFavorite}
//                   color="blue"
//                   className="favorites--icon"
//                 />
//               </div>
//               <div className="Card--priceAndCartWrapper">
//                   <span className="Card--price">{card.price} UAH</span>
//                   <Button
//                       backgroundColor="#000"
//                       text="add to card"
//                       handleClick={() => openModal(card)}
//                       classBtn="Card--addToCardBtn"
//                   />
//               </div>
//         </div>        
//       );
//     }
//   }
  
//   export default Card;
  
//  Card.propTypes = {
//     card: PropTypes.exact({
//       title: PropTypes.string,
//       price: PropTypes.number,
//       url: PropTypes.string,
//       vendorCode: PropTypes.number,
//       color: PropTypes.string,
//       amountAtCart: PropTypes.number,
//       isFavorite: PropTypes.bool
//     }).isRequired,
//     openModal: PropTypes.func.isRequired,
//     addToFavorites: PropTypes.func.isRequired
//   }
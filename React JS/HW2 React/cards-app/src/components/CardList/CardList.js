import React, { Component } from 'react';
import Card from '../Card/Card'
import PropTypes from 'prop-types';

class CardList extends Component {
  render() {
    const {cards, openModal, addToFavorites} = this.props
    
    return (
          <ul className="listItems">
            {cards.map((el, index) => {
                return <Card key={index} card={el} openModal={openModal} addToFavorites={addToFavorites} />
            })}
        </ul>
    );
  }
}

export default CardList;

CardList.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.object).isRequired,
  openModal: PropTypes.func.isRequired,
  addToFavorites: PropTypes.func.isRequired
}



import React, { Component } from 'react';

class Loading extends Component {
  render() {
    return (
      <div style={{textAlign: 'center', marginTop: '70px', fontSize: '35px', fontWeight: 700, fontStyle: 'italic', color: 'darkviolet'}} >
        Loading....
      </div>
    );
  }
}

export default Loading;

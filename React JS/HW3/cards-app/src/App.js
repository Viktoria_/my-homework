import React, { useState, useEffect } from 'react';
import './App.scss';
import axios from 'axios';
import Loading from './components/Loading/Loading';
import Header from './components/Header/Header'
import AppRoutes from './routes/AppRoutes'


const currentModal = {
  header: "Do you want to add this cake to cart?",
  text: "Nice choose! Are you sure?",
  closeButton: true,
  buttonText: {
      confirmButton: "Yes",
      cancelButton: "Cancel"
  }
}

const App = () => {
  const [cards, setCards] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [isActive, setIsActive] = useState(false);
  const [card, setCard] = useState({});
  const [itemAtCart, setItemAtCart] = useState("");
  const [modalHeader, setModalHeader] = useState("");
  const [modalAddOrDeleteValue, setmodalAddOrDeleteValue] = useState("add");

  const normalizeData = (data) => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || []
    const cart = JSON.parse(localStorage.getItem('cart')) || []
    return data.map(elem => {
      elem.isFavorite = favorites.includes(elem.vendorCode)
      let flag = true
      cart.forEach(item => {
        if (item.vendorCode === elem.vendorCode) {
          elem.amountAtCart = item.amountAtCart
          flag = false
        } 
      })
      if (flag) {
        elem.amountAtCart = 0
      }
      return elem
    })
  }

  useEffect(() => {
    axios('http://localhost:3000/items.json')
      .then(result => {
        setCards(normalizeData(result.data.items))
        setLoading(false)
      })
  }, []);

  const openModal = (item, addDelSwitcher=true) => {
    setCard(item)
    setIsActive(true)
    setItemAtCart(item.title)
    addDelSwitcher ? setmodalAddOrDeleteValue("add") : setmodalAddOrDeleteValue("delete")
  }

  useEffect(() => {
    setModalHeader(`Do you want to ${modalAddOrDeleteValue} ${itemAtCart} to or from cart?`)
  }, [itemAtCart, modalAddOrDeleteValue]);

  const closeModal = () => {
    setIsActive(false)
  }

  const addToCart = () => {
    setIsActive(false)
      
    const newCards = cards.map(elem => {
      if (elem.vendorCode === card.vendorCode) {
        elem.amountAtCart++
      }
      return elem
    })
    setCards(newCards)
    let cart = JSON.parse(localStorage.getItem("cart")) || []
    cart = newCards.filter(elem => elem.amountAtCart !== 0)
    localStorage.setItem('cart', JSON.stringify(cart))
  }

  const addToFavorites = (card) => {
    const newCards = cards.map(elem => {
      if (elem.vendorCode === card.vendorCode) {
        elem.isFavorite = !elem.isFavorite
      }
      return elem
    })
    setCards(newCards)

    let favorites = JSON.parse(localStorage.getItem("favorites")) || []
    favorites = (favorites.includes(card.vendorCode) ? favorites.filter(elem => elem !== card.vendorCode) : favorites.concat(card.vendorCode))
    localStorage.setItem("favorites", JSON.stringify(favorites))
  }
    
  const deleteFromCart = () => {
    setIsActive(false)
      
    const newCards = cards.map(elem => {
      if (elem.vendorCode === card.vendorCode) {
        elem.amountAtCart--
      }
      return elem
    })
    setCards(newCards)
    let cart = JSON.parse(localStorage.getItem("cart")) || []
    cart = newCards.filter(elem => elem.amountAtCart !== 0)
    localStorage.setItem('cart', JSON.stringify(cart))
  }

  if (isLoading) {
    return <Loading/>
  } 

  return (
    <div className="App">
      <Header cards={cards}/>
      <AppRoutes 
        cards={cards} 
        openModal={openModal} 
        closeModal={closeModal}
        addToCart={addToCart} 
        deleteFromCart={deleteFromCart}
        isActive={isActive} 
        currentModal={currentModal}
        modalHeader={modalHeader}
        addToFavorites={addToFavorites}
        modalAddOrDeleteValue={modalAddOrDeleteValue}
      />
    </div>
  );
}

export default App;


 
import React from 'react';
import PropTypes from 'prop-types'

const Button = ({backgroundColor, text, handleClick, classButton}) => {
    return (
      <button className={classButton} style={{backgroundColor: backgroundColor}} onClick={handleClick}>{text}
      </button>      
    );
  }


export default Button;


Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired
}

Button.defaultProps = {
  classButton: "button"
}

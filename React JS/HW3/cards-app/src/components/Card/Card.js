import React from "react";
import Button from "../Button/Button";
import Icon from "../Icon/Icon";
import "./Card.scss";
import PropTypes from "prop-types"

const Card = (props) => {
      const {card, openModal, addToFavorites} = props
      return (
      <li className="Card-wrapper">
        <div className="card">
              <img src={card.url} alt={card.title} width='240' height='290'></img>
              <span className="Card--title">{card.title}</span>
              <span>{card.vendorCode}</span>
              <span>{card.color}</span>

              <div className="Card--favorites favorites">
              <span className="favorites--title">Add to favorites</span>
              <Icon
              onClick={() => addToFavorites(card)}
              filled={card.isFavorite}
              color="darkviolet"
              className="favorites--icon"
              />
              </div>

              <div className="Card--priceAndCartWrapper">
              <span className="Card--price">{card.price} UAH</span>
              <Button
                backgroundColor="darkviolet"
                text="add to card"
                handleClick={() => openModal(card)}
                classButton="Card--addToCartButton"
              />  
          
 
             </div>                               
              
        </div> 

        <Button 
        backgroundColor="grey"
        text="X"
        handleClick={() => openModal(card, false)}
        classButton="deleteFromCartButton"
      />

      
      </li>      
      );
    }
  
  
  export default Card;
  
 Card.propTypes = {
    card: PropTypes.exact({
      title: PropTypes.string,
      price: PropTypes.number,
      url: PropTypes.string,
      vendorCode: PropTypes.number,
      color: PropTypes.string,
      amountAtCart: PropTypes.number,
      isFavorite: PropTypes.bool
    }).isRequired,
    openModal: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired
  }
  
import React from 'react';
import Card from '../Card/Card'
import Modal from '../Modal/Modal'
import Button from '../Button/Button'
import './CardList.scss'
import PropTypes from 'prop-types';

const CardList = (props) => {
  
    const {cards, openModal, closeModal, addToCart, deleteFromCart, isActive, currentModal, modalHeader, addToFavorites, modalAddOrDeleteValue} = props;
    return (
      <>
          <ul className="listItems">
            {cards.map((el, index) => {
                return <Card key={index} 
                card={el} 
                openModal={openModal} 
                addToFavorites={addToFavorites} />
            })}
          </ul>

        {isActive && <Modal
        header={modalHeader}
        text={currentModal.text}
        closeButton={currentModal.closeButton}
        actions={{
          confirmButton: () => (<Button 
          text={currentModal.buttonText.confirmButton}
          handleClick={(modalAddOrDeleteValue === "add") ? addToCart : deleteFromCart}
          classButton="buttons-Confirm-Cancel--button"/>),

          cancelButton: () => (<Button
            text={currentModal.buttonText.cancelButton}
            handleClick={closeModal}
            classButton="buttons-Confirm-Cancel--button"/>)
          }}
        isActive={isActive}
        closeModal={closeModal}
      />}
      </>
    );
  }


export default CardList;

CardList.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.object).isRequired,
  openModal: PropTypes.func.isRequired,
  addToFavorites: PropTypes.func.isRequired
}



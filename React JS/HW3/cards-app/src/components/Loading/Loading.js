import React from 'react';

const Loading = () => {
  
    return (
      <div style={{textAlign: 'center', marginTop: '70px', fontSize: '35px', fontWeight: 700, fontStyle: 'italic', color: 'darkviolet'}} >
        Loading....
      </div>
    );
  }


export default Loading;

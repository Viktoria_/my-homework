// import React, { Component } from 'react';
// import './Main.scss';
// import  CardList from '../CardList/CardList'
// import Modal from '../Modal/Modal'
// import Button from '../Button/Button'
// import PropTypes from 'prop-types';

// class Main extends Component {
//   render() {
//     const {cards, openModal, closeModal, addToCart, isActive, currentModal, addToFavorites} = this.props
    
//     return (
//       <main className="Main">
//         <CardList cards={cards} openModal={openModal} addToFavorites={addToFavorites} />
//         {isActive && <Modal
//           header={currentModal.header}
//           text={currentModal.text}
//           closeButton={currentModal.closeButton}
//           actions={{
//             confirmButton: () => (<Button 
//             text={currentModal.buttonText.confirmButton}
//             handleClick={addToCart}
//             classButton="buttons-Confirm-Cancel"/>),

//             cancelButton: () => (<Button
//               text={currentModal.buttonText.cancelButton}
//               handleClick={closeModal}
//               classButton="buttons-Confirm-Cancel"/>)
//             }}
//           isActive={isActive}
//           closeModal={closeModal}
//         />}
//       </main>
//     );
//   }
// }

// export default Main;

// Main.propTypes = {
//   cards: PropTypes.arrayOf(PropTypes.object).isRequired,
//   openModal: PropTypes.func.isRequired,
//   closeModal: PropTypes.func.isRequired,
//   addToCart: PropTypes.func.isRequired,
//   isActive: PropTypes.bool.isRequired,
//   currentModal: PropTypes.object.isRequired,
//   addToFavorites: PropTypes.func.isRequired
// }

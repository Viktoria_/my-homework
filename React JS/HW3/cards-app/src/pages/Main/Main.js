import React from 'react';
import CardList from '../../components/CardList/CardList'
import PropTypes from 'prop-types';

const Main = (props) => {
  const {cards, openModal, closeModal, addToCart, deleteFromCart, isActive, currentModal, modalHeader, addToFavorites, modalAddOrDeleteValue} = props;
    return (
      <CardList 
        cards={cards} 
        openModal={openModal} 
        closeModal={closeModal}
        addToCart={addToCart}
        deleteFromCart={deleteFromCart}
        isActive={isActive}
        currentModal={currentModal}
        modalHeader={modalHeader} 
        addToFavorites={addToFavorites} 
        modalAddOrDeleteValue={modalAddOrDeleteValue} 
      />
    );
}

export default Main;

Main.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.object).isRequired,
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  deleteFromCart: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
  isActive: PropTypes.bool.isRequired,
  currentModal: PropTypes.object.isRequired,
  modalHeader: PropTypes.string.isRequired,
  addToFavorites: PropTypes.func.isRequired,
  modalAddOrDeleteValue: PropTypes.string.isRequired
}

"use strict";

let setPassword = function () {
  const icons = document.querySelectorAll(".icon-password"),
    btn = document.getElementById("button"),
    inputs = document.querySelectorAll(".input");
  let flag = true;
  icons.forEach((el) => {
    el.addEventListener("click", showPassword);
  });
  function showPassword() {
    this.classList.toggle("fa-eye");
    this.classList.toggle("fa-eye-slash");
    if (this.classList.contains("fa-eye")) {
      this.previousElementSibling.setAttribute("type", "password");
    } else {
      this.previousElementSibling.setAttribute("type", "text");
    }
  }

  btn.addEventListener("click", comparePassword);
  function comparePassword() {
    if (inputs[0].value === inputs[1].value) {
      alert("You are welcome");
    } else if (flag) {
      const div = document.createElement("div");
      div.id = "divid";
      div.textContent = "Нужно ввести одинаковые значения!!!";
      div.style.color = "red";
      btn.before(div);
      flag = false;
    }
  }

  inputs.forEach((el) => {
    el.addEventListener("click", deleteDiv);
  });
  function deleteDiv() {
    if (!flag) {
      const div = document.getElementById("divid");
      div.remove();
      flag = true;
    }
  }
};

setPassword();

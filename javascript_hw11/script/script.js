"use strict";

let changeColor = function () {
  const keys = document.querySelectorAll(".btn");

  document.addEventListener("keydown", function (event) {
    keys.forEach((el) => {
      let keyLetter = event.key.slice(0, 1).toUpperCase() + event.key.slice(1);
      if (el.textContent === keyLetter) {
        changeColorBlack();
        el.style.backgroundColor = "#0000ff";
      }
    });
    function changeColorBlack() {
      keys.forEach((el) => {
        el.style.backgroundColor = "#000000";
      });
    }
  });
};
changeColor();

"use strict";

const button = document.getElementById("change-color");
const image = document.getElementsByClassName("img1")[0];
image.before(button);

let newTheme = "",
  serialOldColorTheme = "",
  currentTheme = "";

function getColors() {
  const arrElColor = [
    document.getElementsByClassName("main-menu-link")[0],
    document.getElementsByClassName("h2")[0],
    document.getElementsByClassName("main-content")[0],
    document.getElementsByClassName("button")[0],
    document.getElementsByClassName("page-footer")[0],
  ];
  const arrBgColor = [
    document.getElementsByClassName("page-body")[0],
    document.getElementsByClassName("main-menu")[0],
    document.getElementsByClassName("button")[0],
  ];

  const oldColorTheme = {};
  let i = 0,
    theCSSprop;
  arrElColor.forEach((el) => {
    theCSSprop = window.getComputedStyle(el, null).getPropertyValue("color");
    oldColorTheme[i] = theCSSprop;
    i++;
  });

  arrBgColor.forEach((el) => {
    theCSSprop = window
      .getComputedStyle(el, null)
      .getPropertyValue("background-color");
    oldColorTheme[i] = theCSSprop;
    i++;
  });
  serialOldColorTheme = JSON.stringify(oldColorTheme);
}
getColors();

function changeColors() {
  document.getElementsByClassName("main-menu-link")[0].style.color =
    newTheme[0];
  document.getElementsByClassName("h2")[0].style.color = newTheme[1];
  document.getElementsByClassName("main-content")[0].style.color = newTheme[2];
  document.getElementsByClassName("button")[0].style.color = newTheme[3];
  document.getElementsByClassName("page-footer")[0].style.color = newTheme[4];
  document.getElementsByClassName("page-body")[0].style.backgroundColor =
    newTheme[5];
  document.getElementsByClassName("main-menu")[0].style.backgroundColor =
    newTheme[6];
  document.getElementsByClassName("button")[0].style.backgroundColor =
    newTheme[7];
}

if (
  localStorage.getItem("oldTheme") != null &&
  localStorage.getItem("currentTheme") !== serialOldColorTheme
) {
  currentTheme = localStorage.getItem("currentTheme");
  newTheme = JSON.parse(currentTheme);
  localStorage.setItem("oldTheme", serialOldColorTheme);
  changeColors();
}

button.addEventListener("click", function switchColorTheme() {
  getColors();
  if (localStorage.getItem("oldTheme") == null) {
    newTheme = {
      0: "rgb(0,0,0)",
      1: "rgb(255,255,255)",
      2: "rgb(255, 255, 255)",
      3: "rgb(255, 204, 255)",
      4: "rgb(102, 0, 102)",
      5: "rgb(255,102,204)",
      6: "rgb(255,204,255)",
      7: "rgb(102, 0, 102)",
    };
    currentTheme = JSON.stringify(newTheme);
    localStorage.setItem("currentTheme", currentTheme);
    localStorage.setItem("oldTheme", serialOldColorTheme);
  } else {
    currentTheme = localStorage.getItem("oldTheme");
    newTheme = JSON.parse(currentTheme);
    const temtTheme = localStorage.getItem("currentTheme");
    localStorage.setItem("oldTheme", temtTheme);
    localStorage.setItem("currentTheme", currentTheme);
  }
  changeColors();
});

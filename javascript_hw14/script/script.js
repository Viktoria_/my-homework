"use strict";

$(document).ready(function () {
  $("#menu").on("click", "a", function (event) {
    event.preventDefault();
    let id = $(this).attr("href"),
      top = $(id).offset().top;
    $("body,html").animate({ scrollTop: top }, 6000);
  });
});

$(document).ready(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() >= document.documentElement.clientHeight) {
      $("#up").css("display", "block");
    } else {
      $("#up").css("display", "none");
    }
  });

  $("#up").click(function () {
    $("body,html").animate({ scrollTop: 0 }, 3000);
    return false;
  });
});

$(document).ready(function () {
  $("#switcher").click(function () {
    $(".posts__section").slideToggle();
  });
});

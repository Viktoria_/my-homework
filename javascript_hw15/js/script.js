"use strict";
let userNum = prompt("Введите ваше число для вычислений");

function factorial(n) {
  if (n === null || isNaN(+n) || n === "") {
    prompt("Ошибка! Вы ввели не число, пожалуйста, введите число", n);
  } else if (n == 1) {
    return 1;
  } else return n * factorial(n - 1);
}
alert(factorial(userNum));

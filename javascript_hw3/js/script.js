"use strict";

let a = prompt("Введите первое число");
let b = prompt("Введите второе число");
let op = prompt("Выберите необходимое действие +, -, *, /");

function mathOper(a, b, op) {
  if (op == "+") {
    console.log(+a + +b);
    return;
  } else if (op == "-") {
    console.log(a - b);
    return;
  } else if (op == "/") {
    console.log(a / b);
    return;
  } else if (op == "*") {
    console.log(a * b);
    return;
  } else if (
    isNaN(+a) ||
    typeof a != "number" ||
    isNaN(+b) ||
    typeof b != "number" ||
    op != "+" ||
    op != "-" ||
    op != "*" ||
    op != "/" ||
    op != "+"
  ) {
    console.log("Ошибка! Вы ввели неправильное значение! Попробуйте еще раз!");
  }
  return;
}
mathOper(a, b, op);

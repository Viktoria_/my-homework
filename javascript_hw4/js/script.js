"use strict";

function createNewUser() {
  let firstName, lastName;
  do {
    firstName = prompt("Enter your first name");
  } while (firstName === null || firstName === "" || firstName.length < 2);
  do {
    lastName = prompt("Enter your last name");
  } while (lastName === null || lastName === "" || lastName.length < 2);

  const newUser = {
    firstName,
    lastName,
    getLogin() {
      console.log(
        this.firstName.trim().slice(0, 1).toLocaleLowerCase() +
          this.lastName.trim().toLocaleLowerCase()
      );
    },
    propertyChange() {
      if (propertyTitle in this) {
        this(propertyTitle) = value;
      } else {
        console.log("Error");
      }
    },
  };

  newUser.getLogin();
  return newUser;
}

createNewUser();

"use strict";

function createNewUser() {
  let firstName, lastName, birthday;
  let now = new Date();
  do {
    firstName = prompt("Enter your first name");
  } while (firstName === null || firstName === "" || firstName.length < 2);
  do {
    lastName = prompt("Enter your last name");
  } while (lastName === null || lastName === "" || lastName.length < 2);
  do {
    birthday = prompt('Enter your date of birthday  "dd.mm.yyyy"');
  } while (birthday === null);

  let yearBirthday = +birthday.slice(6);

  const newUser = {
    firstName,
    lastName,
    birthday,
    getPassword() {
      return (
        this.firstName.trim().slice(0, 1).toLocaleUpperCase() +
        this.lastName.trim().toLocaleLowerCase() +
        this.birthday.trim().substring(6)
      );
    },
    getAge() {
      return now.getFullYear() - yearBirthday;
    },
  };

  console.log("Your age is  " + newUser.getAge() + " years old ");
  console.log("Your password   " + newUser.getPassword());
  return newUser;
}

createNewUser();

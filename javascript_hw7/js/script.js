"use strict";
const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
function makeListFromArr(arr, docBody = document.body) {
  console.log(arr);
  console.log(docBody);
}
const list = document.getElementById("list");
document.body.appendChild(list);

const fragmentHTML = document.createDocumentFragment();
const arrItems = arr.map((el) => {
  let item = document.createElement("li");
  item.textContent = el;
  fragmentHTML.append(item);
  return item;
});

list.append(fragmentHTML);

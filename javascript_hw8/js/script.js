"use strict";
let span1 = document.createElement("span1");
let span2 = document.createElement("span2");
let btnX = document.createElement("button");
let input = document.getElementById("input");

input.addEventListener("focus", changeBorderColor);
function changeBorderColor(e) {
  input.style.outlineColor = "green";
}

input.addEventListener("blur", changeColorBorder);
function changeColorBorder(e) {
  document.body.append(span2);
  document.body.prepend(span1, btnX);
  if (input.value < 0 || input.value === "") {
    input.style.border = "solid";
    input.style.borderColor = "red";
    span2.innerHTML = "Please enter correct price";
    span1.textContent = "";
    span2.style.color = "red";
    btnX.remove();
  } else {
    span2.textContent = " ";
    span1.textContent = `This price:   ${input.value}`;
    btnX.textContent = "x";
    btnX.style.borderRadius = "10px";
    btnX.style.background = "none";
  }
  btnX.addEventListener("click", function () {
    btnX.remove();
    span1.textContent = "";
    input.value = "";
    span2.innerHTML = "";
  });
}
